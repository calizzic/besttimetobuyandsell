class Solution(object):
    def maxProfit(self, prices):
        """
        :type prices: List[int]
        :rtype: int
        """
        maxProfit = 0
        buy = 0
        sell = 1
        min = prices[0]
        while(sell<len(prices)):
            if(prices[buy]<min):
                min = prices[buy]
            if(prices[sell] - min>maxProfit):
                maxProfit = prices[sell]-min
            buy +=1
            sell +=1
        return maxProfit